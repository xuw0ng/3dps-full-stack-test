3dprintingsystems Full-stack Developer test (02/04/2018)
-------------------------

Make sure you read **all** of this document carefully, and follow the guidelines in it.

## Background

Build a [Hacker News](https://news.ycombinator.com/) like app but for lighting talk polling.

Sorry no mock here, please make a simple and beautiful page that get the job done. We don't mind whether it looks as great as **Hacker News**.

## User Story

1. User open the page and could see a list of lighting talks order by rating submitted by other user;
2. If there's no lighting talk, simply put a placeholder text and encourage user to submit their own talks;
3. The user could vote for the lighting talk by clicking the voter button or icon;
4. After voting the user will get an updated version of the lighting talk list(order by rating);
5. User could always submit a lighting talk with `title`, `description`, and `username`;
6. The user could see his lighting talk on the list after submitting; 

### Functionality

* The **front-end** part should be a single page application rendered in the front-end and load data from restful api(**not** rendered from back-end);
* There should be a **back-end** and database to store the lighting talks

## Getting started

There's nothing here, we leave it to you to choose the build tool, code structure, testing approach...

## Requirements

- Use **React and Redux** to build the front-end part. You can build the component by your own or use existing components provided by the community, like [Material UI](https://github.com/callemall/material-ui)

- Use **express.js** framework to build the back-end http server.

- Use **MongoDB** as the database.

- With clear **documentation** on how to run the code.

- Use **git** to manage code.


## What We Care About

We care about your ability to solve the problem by using the specific libraries. If you are not familiar with these yet, we hope you are a quick learner.

We're interested in your method and how you approach the problem just as much as we're interested in the end result.

Here's what you should aim for:

- Good use of current HTML, CSS and JavaScript & performance best practices.

- Good use of React/Redux/Express/MongoDB.

- Good code style, like **JSCodeStandard**.

- Extensible code.

## Q&A

* Where should I send back the result when I'm done?

Fork this repo and send us a pull request when you think you are done. We don't have deadline for the task. You can make yourself ready before you start. But once you started, we hope you can finish this in three days (or 24 working hours, assuming 8 hours per day).

* What if I have question?

Create a new issue in the repo and we will get back to you very quickly.
